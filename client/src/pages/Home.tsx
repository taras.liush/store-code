import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import ProductTable from '../components/productsTable'
import { AppState, USER_AUTH, USER_UNAUTH } from '../types'
import { useHistory } from 'react-router-dom'

export default function Home() {
  const auth = useSelector((state: AppState) => state.auth)
  const history = useHistory()
  const dispatch = useDispatch()

  React.useEffect(() => {
    const token = localStorage.getItem('token')
    if (!token) {
      dispatch({ type: USER_UNAUTH })
      history.push('/login')
    }
    dispatch({ type: USER_AUTH })
  }, [auth, history])

  const onLogout = () => {
    localStorage.removeItem('token')
    dispatch({ type: USER_UNAUTH })
    // history.push('/login')
  }

  return (
    <div>
      <button onClick={onLogout}>Logout</button>
      <ProductTable />
    </div>
  )
}
// 123
