import React from 'react'
import { Link } from 'react-router-dom'
import { GoogleLogin, GoogleLoginResponse } from 'react-google-login'
import ProductTable from '../components/productsTable'
import { useDispatch } from 'react-redux'
import { USER_AUTH } from '../types'
import { useHistory } from 'react-router-dom'

export default function Login() {
  const dispatch = useDispatch()
  const history = useHistory()

  const handleSuccess = async (res: GoogleLoginResponse) => {
    const rawResponse = await fetch('http://localhost:8080/api/v1/auth', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8',
      },
      body: JSON.stringify({ googleId: res.googleId, token: res.tokenId }),
    })
    const realResponse = await rawResponse.json()
    console.log(realResponse.email)
    localStorage.setItem('token', realResponse.token)
    dispatch({ type: USER_AUTH })
    history.push('/')
  }

  return (
    <div>
      <div>Login</div>
      <GoogleLogin
        clientId="550890630184-gbs9q4c0viqr2nmt2n04c08momogpht2.apps.googleusercontent.com"
        buttonText="Login"
        //Google login package has wrong interface!!!
        //@ts-ignore
        onSuccess={handleSuccess}
        cookiePolicy={'single_host_origin'}
      />
    </div>
  )
}
