import React from 'react'
import { useParams } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { AppState } from '../types'


export default function Product() {
  const { name } = useParams<any>()

  const product = useSelector((state: AppState) =>
    state.products.find((c) => c.name === name)
    
  )

  if (!product) {
    return <div>Product not found</div>
  }

  return (
    <>
      <h1>Product page</h1>
      <ul>
       
        <li>{product.name}</li>
        
      </ul>
    </>
  )
}