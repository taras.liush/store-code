import React from 'react'
import {REMOVE_PRODUCT, ADD_PRODUCT ,AppState} from '../types'
import { useDispatch, useSelector } from 'react-redux'
import {addProducts} from '../redux/actions/products'
import { Link } from 'react-router-dom'

export default function ProductTable() {
  const cart = useSelector((state: AppState) => state.product.inCart)
  const dispatch = useDispatch()
  const products: any[] = useSelector((state: AppState) => state.products)
  React.useEffect(() => {
    fetch('http://localhost:8080/api/v1/products')
      .then((res) => res.json())
      .then((result) => {
        dispatch(addProducts(result))
      })
  }, [dispatch])



  
  return (
    <div>
      {products.map((product) => {
        
        return(
          <div>
          <ul>
            {/* <li>{product.name}</li> */}
            <li className="name">
                <Link to={'/' + product.name}>{product.name}</Link>
              </li>
            <button  onClick={() => {
              dispatch({ type: ADD_PRODUCT, payload: { product: product } })
            }}>Add in cart</button>
          </ul>
          
          </div>
        )
      })}
      <ul>
            {cart.map((product) => {
             return (
              <ul>
              <li>{product.name}</li>
              <button  onClick={() => {
                dispatch({ type: REMOVE_PRODUCT, payload: { product: product } })
              }}>Add in cart</button>
            </ul> 
             
             )
            })}
          </ul>
    </div>
  )
 
}
