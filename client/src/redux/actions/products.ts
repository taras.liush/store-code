import{
    ADD_PRODUCTS,
    AddProductsAction,
    ADD_PRODUCT,
    REMOVE_PRODUCT,
    ProductActions,
} from '../../types'
import ProductDocument from '../../types'

export function addProducts(products: ProductDocument[]): AddProductsAction {
    return {
      type: ADD_PRODUCTS,
      payload: {
        products,
      },
    }
  }

export function addProduct(product: ProductDocument): ProductActions {
  return {
    type: ADD_PRODUCT,
    payload: {
      product,
    }
  }
}

export function removeProduct(product: ProductDocument): ProductActions {
  return {
    type: REMOVE_PRODUCT,
    payload: {
      product,
    }
  }
}