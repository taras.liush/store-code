import { USER_AUTH, USER_UNAUTH } from '../../types'

export function user(
    state: boolean = false,
    action: {type: string}
    ){
    switch (action.type) {
        case USER_AUTH:
        return true
        case USER_UNAUTH:
            return false
        default:
            return state
    }
}
