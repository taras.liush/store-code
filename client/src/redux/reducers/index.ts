import { combineReducers } from 'redux'
import {products} from './products'
import {product} from './product'
import {user} from './user'

const createRootReducer = () =>
  combineReducers({
    product,
    products,
    auth: user
  })

export default createRootReducer
