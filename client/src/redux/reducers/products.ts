import {AddProductsAction, ADD_PRODUCTS } from '../../types'
import ProductDocument from '../../types'

export function products(
    state: ProductDocument[] = [],
    action: AddProductsAction
    ){
    switch (action.type) {
    case ADD_PRODUCTS:
        const { products } = action.payload
        return products
    default:
        return state
    }
}
