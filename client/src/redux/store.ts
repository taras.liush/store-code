
import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import createRootReducer from './reducers'
import {AppState, ProductState} from '../types'

const initState: AppState = {
  product: {
    inCart: []
  },
    products: [],
    auth: false
}

export default function makeStore(initialState = initState) {
    const middlewares = [thunk]
    let composeEnhancers = compose

  if (process.env.NODE_ENV === 'development') {
    if ((window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) {
      composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    }
  }
    
    const store = createStore(
      createRootReducer(),
      // initialState,
     composeEnhancers(applyMiddleware(...middlewares))
    )
  
  
    return store
  }