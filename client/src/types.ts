export const ADD_PRODUCTS = 'ADD_PRODUCTS'
export const ADD_PRODUCT = 'ADD_PRODUCT'
export const REMOVE_PRODUCT = 'REMOVE_PRODUCT'
export const USER_AUTH = 'USER_AUTH'
export const USER_UNAUTH = 'USER_UNAUTH'

export default interface ProductDocument {
    name: string;
    description: string;
    categories: string[];
    variants: string[];
    sizes: string[];
    items: number;
    price: number;
}


export type AddProductsAction = {
    type: typeof ADD_PRODUCTS
    payload: {
      products: ProductDocument[]
    }
  }

export type AddProductAction = {
  type: typeof ADD_PRODUCT
  payload: {
    product: ProductDocument
  }
}

export type RemoveProductAction = {
  type: typeof REMOVE_PRODUCT
  payload: {
    product: ProductDocument
    
  }
}

export type ProductActions = AddProductAction | RemoveProductAction

export type ProductState = {
  inCart: ProductDocument[]
}

export type AppState = {
    product: ProductState
    products: ProductDocument[]
    auth: boolean
}