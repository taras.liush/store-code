import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './pages/Home'
import Login from './pages/Login'
import Product from './pages/Product'
const Routes = () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route exact path="/:login" component={Login} />
    <Route exact path="/:name" component={Product}/>
  </Switch>
)

export default Routes
