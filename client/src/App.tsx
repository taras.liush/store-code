import React from "react";
import { Provider } from "react-redux";
import "./App.css";
// import ProductsTable from './components/productsTable'
import Routes from "./Routes";

function App() {
  return(
    <div>
      <Routes/>
    </div>
  )
}

export default App;
