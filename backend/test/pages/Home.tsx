import React, { useState } from 'react'
import ProductTable from '../components/productTable/ProductTable'


export default function Home() {
  return (  
    <div>
      <ProductTable />
    </div>
  )
}
